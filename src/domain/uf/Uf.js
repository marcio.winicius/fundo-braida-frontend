import axios from 'axios';
import cep from 'cep-promise'

export default class Uf {
    static ufs = [
        "AC",
        "AL",
        "AM",
        "AP",
        "BA",
        "CE",
        "DF",
        "ES",
        "GO",
        "MA",
        "MG",
        "MS",
        "MT",
        "PA",
        "PB",
        "PE",
        "PI",
        "PR",
        "RJ",
        "RN",
        "RO",
        "RR",
        "RS",
        "SC",
        "SE",
        "SP",
        "TO"
    ]

    static consultCep(cp) {
        // return cep(cp).then(response => {
        //     return {
        //         logradouro: response.data.street,
        //         bairro: response.data.neighborhood,
        //         localidade: response.data.city,
        //         uf: response.data.state
        //     }
        // })
        return axios
            .get("https://viacep.com.br/ws/" + cp + "/json/", { headers: { 'Access-Control-Allow-Headers': '*' } }).then(response => response.data);
        // return axios
        //     .get("http://apps.widenet.com.br/busca-cep/api/cep/" + cep + ".json").then(response => {
        //         return {
        //             logradouro: response.data.address,
        //             bairro: response.data.district,
        //             localidade: response.data.city,
        //             uf: response.data.state
        //         }
        //     });
        // return axios("https://viacep.com.br/ws/" + cep + "/json/", {
        //     method: 'GET',
        //     mode: 'no-cors',
        //     headers: {
        //         'Access-Control-Allow-Origin': '*',
        //         'Content-Type': 'application/json',
        //     },
        //     withCredentials: true,
        //     credentials: 'same-origin',
        // });
    }
}