import axios from 'axios';
import User from "@/domain/user/User";
import Bank from "@/domain/bank/Bank";
import Role from "@/domain/role/Role";

export default class UserService {

    list(limit, page) {
        return axios
            .get("/users?limit=" + limit + "&page=" + page).then(response => {
                var users = [];
                response.data.data.forEach(user => {
                    users.push(new User(
                        user.id,
                        user.name,
                        user.cpf,
                        user.email,
                        '',
                        user.address_street,
                        user.address_neighborhood,
                        user.address_city,
                        user.address_state,
                        user.address_zipcode,
                        user.signed_contract,
                        new Bank(user.bank.data.id, user.bank.data.bank_name, user.bank.data.bank_number, user.bank.data.account,
                            user.bank.data.account_digit, user.bank.data.agency, user.bank.data.agency_digit),
                        user.roles.data
                    ));
                });
                return users;
            });
    }
    get(id) {
        return axios
            .get("/users/" + id).then(response => {
                return new User(
                    response.data.data.id,
                    response.data.data.name,
                    response.data.data.cpf,
                    response.data.data.email,
                    '',
                    response.data.data.address_street,
                    response.data.data.address_neighborhood, response.data.data.address_city,
                    response.data.data.address_state,
                    response.data.data.address_zipcode,
                    response.data.data.signed_contract,
                    new Bank(
                        response.data.data.bank.data.id,
                        response.data.data.bank.data.bank_name,
                        response.data.data.bank.data.bank_number,
                        response.data.data.bank.data.account,
                        response.data.data.bank.data.account_digit,
                        response.data.data.bank.data.agency,
                        response.data.data.bank.data.agency_digit
                    ),
                    response.data.data.roles.data
                );
            });
    }

    create(user) {
        var params = {
            name: user.name,
            email: user.email,
            cpf: user.cpf,
            address_zipcode: user.address_zipcode,
            address_state: user.address_state,
            address_city: user.address_city,
            address_neighborhood: user.address_neighborhood,
            address_street: user.address_street,
            bank: {
                bank_id: user.bank.bank_id,
                account: user.bank.account,
                account_digit: user.bank.account_digit,
                agency: user.bank.agency,
                agency_digit: user.bank.agency_digit
            }
        };
        console.log("Create user");
        console.log(params);
        return axios
            .post("/users", params).then(response => response.data.data);
    }
    update(id, user) {
        var rolesIds = [];
        user.roles.forEach(role => {
            rolesIds.push(role.id);
        })
        var params = {
            name: user.name,
            email: user.email,
            cpf: user.cpf,
            address_zipcode: user.address_zipcode,
            address_state: user.address_state,
            address_city: user.address_city,
            address_neighborhood: user.address_neighborhood,
            address_street: user.address_street,
            bank: {
                bank_id: user.bank.bank_id,
                account: user.bank.account,
                account_digit: user.bank.account_digit,
                agency: user.bank.agency,
                agency_digit: user.bank.agency_digit
            },
            role_ids: rolesIds
        };
        console.log("Update user");
        console.log(params);
        return axios
            .put("/users/" + id, params).then(response => {
                var user = response.data.data;
                return new User(
                    user.id,
                    user.name,
                    user.cpf,
                    user.email,
                    '',
                    user.address_street,
                    user.address_neighborhood,
                    user.address_city,
                    user.address_state,
                    user.address_zipcode,
                    user.signed_contract,
                    new Bank(user.bank.data.id, user.bank.data.bank_name, user.bank.data.bank_number, user.bank.data.account,
                        user.bank.data.account_digit, user.bank.data.agency, user.bank.data.agency_digit),
                    user.roles.data
                )
            });
    }

    delete(id) {
        console.log('Delete user ' + id)
        return axios
            .delete("/users/" + id);
    }
}