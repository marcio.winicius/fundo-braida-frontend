import Bank from "@/domain/bank/Bank";
import Role from "@/domain/role/Role";

export default class User {
    constructor(
        id = '',
        name = '',
        cpf = '',
        email = '',
        password = '',
        address_street = '',
        address_neighborhood = '',
        address_city = '',
        address_state = '',
        address_zipcode = '',
        signed_contract = {},
        bank = new Bank(),
        roles = []
    ) {
        this.id = id;
        this.name = name;
        this.cpf = cpf;
        this.email = email;
        this.password = password;
        this.address_street = address_street;
        this.address_neighborhood = address_neighborhood;
        this.address_city = address_city;
        this.address_state = address_state;
        this.address_zipcode = address_zipcode;
        this.signed_contract = signed_contract;
        this.bank = bank;
        this.roles = roles;
    }
}