import axios from "axios";
import Bank from "@/domain/bank/Bank";

export default class BankService {

    list(limit, page) {
        return axios
            .get("/banks?limit=" + limit + "&page=" + page).then(response => {
                let banks = [];
                response.data.data.forEach(bk => {
                    banks.push(new Bank(
                        bk.id,
                        bk.name,
                        bk.number
                    ));
                });
                return banks;
            });
    }

    get(id) {
        return axios
            .get("/banks/" + id).then(response => new Bank(response.data.data.id, response.data.data.name, response.data.data.number));
    }

    create(bank) {
        return axios
            .post("/banks", {
                number: bank.bank_number,
                name: bank.bank_name
            }).then(response => new Bank(response.data.data.id, response.data.data.name, response.data.data.number));
    }
    update(id, bank) {
        return axios
            .put("/banks/" + id, {
                number: bank.bank_number,
                name: bank.bank_name
            }).then(response => new Bank(response.data.data.id, response.data.data.name, response.data.data.number));
    }

    delete(id) {
        console.log('Deletando banco ' + id);
        return axios
            .delete("/banks/" + id).then(response => response.data.data);
    }


}