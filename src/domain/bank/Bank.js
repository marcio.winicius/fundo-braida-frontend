export default class Bank {
    constructor(
        bank_id = 0,
        bank_name = "",
        bank_number,
        account = "",
        account_digit = "",
        agency = "",
        agency_digit = ""
    ) {
        this.bank_id = bank_id;
        this.bank_name = bank_name;
        this.bank_number = bank_number;
        this.account = account;
        this.account_digit = account_digit;
        this.agency = agency;
        this.agency_digit = agency_digit;
    }
}