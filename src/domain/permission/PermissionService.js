import axios from 'axios';

export default class PermissionService {

    list(limit, page) {
        return axios
            .get("/permissions?limit=" + limit + "&page=" + page).then(response => {
                console.log(JSON.stringify(response.data));
                return response.data.data
            });
    }
    get(id) {
        return axios
            .get("/permissions/" + id).then(response => response.data.data);
    }

    create(permission) {
        return axios
            .post("/permissions", {
                name: permission.name,
                readable_name: permission.readable_name
            }).then(response => response.data.data);
    }
    update(id, permission) {
        return axios
            .put("/permissions/" + id, {
                name: permission.name,
                readable_name: permission.readable_name
            }).then(response => response.data.data);
    }

    delete(id) {
        return axios
            .delete("/permissions/" + id).then(response => response.data.data);
    }
}