export default class Permission {
    constructor(
        id = 0,
        name = '',
        readable_name = ''
    ) {
        this.id = id;
        this.name = name;
        this.readable_name = readable_name;
    }
}