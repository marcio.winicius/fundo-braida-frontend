import axios from 'axios';
import User from "@/domain/user/User";
import Bank from "@/domain/bank/Bank";
import Role from "@/domain/role/Role";
import Permission from "@/domain/permission/Permission";

export default class AuthService {

    login(email, password) {
        return axios.post("/auth/login", { login: email, password: password }).then(response => {
            var user = response.data.data;
            var roles = [];
            user.roles.data.forEach(role => {
                var permissions = [];
                role.permissions.data.forEach(permission => {
                    permissions.push(new Permission(permission.id, permission.name, permission.readable_name));
                })
                roles.push(new Role(role.id, role.name, permissions));
            });
            user = new User(
                user.id,
                user.name,
                user.cpf,
                user.email,
                "",
                user.address_street,
                user.address_neighborhood,
                user.address_city,
                user.address_state,
                user.address_zipcode,
                user.signed_contract,
                new Bank(
                    user.bank.data.id,
                    user.bank.data.bank_name,
                    user.bank.data.bank_number,
                    user.bank.data.account,
                    user.bank.data.account_digit,
                    user.bank.data.agency,
                    user.bank.data.agency_digit
                ),
                roles
            );
            user.token = response.data.data.token;
            return user;
        });
    }
}