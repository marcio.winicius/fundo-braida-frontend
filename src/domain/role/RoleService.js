import axios from 'axios';
import Role from "@/domain/role/Role";
import Permission from "@/domain/permission/Permission";

export default class RoleService {

    list(limit, page) {
        return axios
            .get("/roles?limit=" + limit + "&page=" + page).then(response => {
                var roles = [];
                response.data.data.forEach(role => {
                    roles.push(new Role(role.id, role.name, role.permissions.data));
                });
                return roles;
            });
    }
    get(id) {
        return axios
            .get("/roles/" + id).then(response => {
                return new Role(
                    response.data.data.id,
                    response.data.data.name,
                    response.data.data.permissions.data
                );
            });
    }

    create(role) {
        var permissionsIds = [];
        role.permissions.forEach(p => {
            permissionsIds.push(p.id);
        });
        var params = {
            name: role.name,
            permission_ids: permissionsIds
        };
        console.log("Create role");
        console.log(params);
        return axios
            .post("/roles", params).then(response => {
                return new Role(response.data.data.id, response.data.data.name, response.data.data.permissions.data);
            });
    }
    update(id, role) {
        var permissionsId = [];
        role.permissions.forEach(p => {
            permissionsId.push(p.id);
        })
        var params = {
            id: id,
            name: role.name,
            permission_ids: permissionsId
        };
        console.log("Update role id " + id);
        console.log(params);
        return axios
            .put("/roles/" + id, params).then(response => {
                var permissions = [];
                response.data.data.permissions.data.forEach(p => {
                    permissions.push(new Permission(p.id, p.name, p.readable_name));
                });
                return new Role(
                    response.data.data.id, response.data.data.name, permissions
                );
            });
    }

    delete(id) {
        console.log("Delete role " + id)
        return axios
            .delete("/roles/" + id).then(response => response.data.data);
    }
}