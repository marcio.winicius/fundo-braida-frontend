/*!
 *
 * Angle - Bootstrap Admin Template
 *
 * Version: 4.1.1
 * Author: @themicon_co
 * Website: http://themicon.co
 * License: https://wrapbootstrap.com/help/licenses
 *
 */

import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import VueI18Next from '@panter/vue-i18next';
import axios from 'axios';
import VueLocalStorage from 'vue-localstorage'
import VeeValidate from 'vee-validate';
import Notifications from 'vue-notification'

import './vendor.js'

import App from './App.vue'
import router from './router'
import i18next from './i18n.js';

Vue.use(BootstrapVue);
Vue.use(VueI18Next);
Vue.use(VueLocalStorage)
Vue.use(VeeValidate, {
    fieldsBagName: 'formFields'  // fix issue with b-table
})
Vue.use(Notifications)

Vue.config.productionTip = false
axios.defaults.baseURL = "http://68.183.105.80/api/v1";
axios.defaults.headers.common = { 'Authorization': "Bearer " + Vue.localStorage.get("token"), 'Content-Type': 'application/json', 'Accept': 'application/json' };
// axios.defaults.headers.common = { 'Authorization': "Bearer " + Vue.localStorage.get("token"), 'Content-Type': 'application/x-www-form-urlencoded', 'Accept': 'application/json' };

const i18n = new VueI18Next(i18next);

new Vue({
    i18n,
    router,
    render: h => h(App)
}).$mount('#app')
