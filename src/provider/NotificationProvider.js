export default class NotificationProvider {

    constructor(notify) {
        this.notify = notify;
    }
    success(title, text) {
        this.notify({
            group: "APP",
            title,
            text,
            type: "bg-success text-white"
        });
    }

    info(title, text) {
        this.notify({
            group: "APP",
            title,
            text,
            type: "bg-info text-white"
        });
    }

    primary(title, text) {
        this.notify({
            group: "APP",
            title,
            text,
            type: "bg-primary text-white"
        });
    }
    default(title, text) {
        this.notify({
            group: "APP",
            title,
            text,
            type: "bg-default text-white"
        });
    }

    warning(title, text) {
        this.notify({
            group: "APP",
            title,
            text,
            type: "bg-warning text-white"
        });
    }

    error(title, text) {
        this.notify({
            group: "APP",
            title,
            text,
            type: "bg-danger text-white"
        });
    }
}