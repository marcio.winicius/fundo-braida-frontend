# Fundo Braida

## Pre requistos

### Instalação do NodeJS
Baixa e instale o nodejs para o seu sistema operacional
https://nodejs.org/en/download/

### Instalação do VueJS
Abrir o terminal e executar o seguinte comando para instalação do VueJS
```
npm install vue -g
```

## Alterar o endpoint do webservice (se necessário)
Acesse o arquivo 'src/main.js' e altere a propriedade 'axios.defaults.baseURL' para a URL desejada.

## Rodando a aplicação
Acesse o local da aplicação com o terminal para executar os comandos a seguir

### Atualizando dependencias
```
npm install
```

### Rodando em ambiente de desenvolvimento
```
npm run serve
```

### Gerando a build para produção
```
npm run build
```

### Rodando em ambiente de produção
```
npm start ./dist
```
